winner = checkWinner(GameState);
% Check if there is a winner in the game
if winner ~= 0
    % Update the Records
    Records(winner) = Records(winner) + 1;
    % Update the UI
    set(findobj('tag', 'TurnTB'), 'string', [PlayerNames{winner}, ' Won']);
    UpdateRecords(Records);
    SetInactive();
    % Sets the background to default
    SetPlayerNameBG(0);
    if winner == 1
        open 'WinningPicture.fig';
    else
        open 'LosingPicture.fig';
    end
else
    % Check if there are ties in a game
    if all(GameState)
        % Update the records
        Records(3) = Records(3) + 1;
        % Update the UI
        set(findobj('tag', 'TurnTB'), 'string', 'Tie!');
        UpdateRecords(Records);
        % Sets the background to default
        SetPlayerNameBG(0);
        % NOTE: Set Inactive not needed since if there is a tie
        %       then it is assumed that everything is set to
        %       inactive
        % Set to 3 to reprent a tie in the winner variable
        winner = 3;
    end
end