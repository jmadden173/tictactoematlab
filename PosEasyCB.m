% -------------------------------------------------------------------------
% IF THE PLAYER GOES FIRST
% -------------------------------------------------------------------------
if CurrentMove == 1
    % ---------------------------------------------------------------------
    % PLAYERS MOVE
    % ---------------------------------------------------------------------
    % Sets the current PB to inactive
    set(gco, 'Enable', 'inactive')

    % Get which PB the user played on and updates the gamestate accordingly
    GameState(get(gco, 'UserData')) = CurrentMove;

    % Displays the x's and o's on the push buttons
    if CurrentMove == 1
        set(gco, 'string', 'x')
        CurrentMove = 2;
    else
        set(gco, 'string', 'o')
        CurrentMove = 1;
    end

    % Update the UI with whos move it is
    SetTurnTB(PlayerNames{CurrentMove});



    % ---------------------------------------------------------------------
    % CHECK FOR WINNER AFTER THE USER MOVE
    % ---------------------------------------------------------------------
    UpdateWin();


    % Check if someone other than the computer has won or there is a tie
    if ~(winner == 3 || winner == 1)
        % -----------------------------------------------------------------
        % COMPUTERS MOVE
        % Ref:  MoveEasy.m
        % -----------------------------------------------------------------
        MoveEasy();
        
        % -----------------------------------------------------------------
        % CHECK FOR WINNER AT END
        % -----------------------------------------------------------------
        UpdateWin();
    end
    
    
    
    
    
% -------------------------------------------------------------------------
% IF THE COMPUTER GOES FIRST
% -------------------------------------------------------------------------
else
        % -----------------------------------------------------------------
        % COMPUTERS MOVE
        % Ref:  MoveEasy.m
        % -----------------------------------------------------------------
        MoveEasy();




        % -------------------------------------------------------------------------
        % CHECK FOR WINNER AT END
        % -------------------------------------------------------------------------
        UpdateWin();
    
    
    
    % Check if the computer has won or theres a tie
    if ~(winner == 3 || winner == 2)
        % -------------------------------------------------------------------------
        % PLAYERS MOVE
        % -------------------------------------------------------------------------
        % Sets the current PB to inactive
        set(gco, 'Enable', 'inactive')

        % Get which PB the user played on and updates the gamestate accordingly
        GameState(get(gco, 'UserData')) = CurrentMove;

        % Displays the x's and o's on the push buttons
        if CurrentMove == 1
            set(gco, 'string', 'x')
            CurrentMove = 2;
        else
            set(gco, 'string', 'o')
            CurrentMove = 1;
        end

        % Update the UI with whos move it is
        SetTurnTB(PlayerNames{CurrentMove});




        % -------------------------------------------------------------------------
        % CHECK FOR WINNER BEFORE THE USER MOVES
        % -------------------------------------------------------------------------
        UpdateWin();

    end
    
    
end