% Find where the game state is not equal to 0
playable = find(~GameState);

% Get where to play
CompMove = playable(randi(length(playable)));

set(findobj('UserData', CompMove), 'Enable', 'inactive');

% Displays the x's and o's on the push buttons
if CurrentMove == 1
    set(findobj('UserData', CompMove), 'string', 'x');
    GameState(CompMove) = 1;
    CurrentMove = 2;
else
    set(findobj('UserData', CompMove), 'string', 'o');
    GameState(CompMove) = 2;
    CurrentMove = 1;
end

% Update the UI with whos move it is
SetTurnTB(PlayerNames{CurrentMove});