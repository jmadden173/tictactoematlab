% Filename: CreditsCB
% Purpose: This is the callback to the credits pushbutton in the main menu.
% This displays a figure showing the credits.

open 'Credits.fig'