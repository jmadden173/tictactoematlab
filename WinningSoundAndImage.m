% filename: WinningSoundAndImage
% congratulatory sound at the end of the game

[Wintinks, WinFreq] = audioread('Trumphet.wav');
WinWavObj = audioplayer(Wintinks, WinFreq);
play(WinWavObj)

CDImage = imread('ChickenDinner.jpg','jpeg');
axes('position',[0 0 1 1])
image(CDImage)
set(gca, 'visible','off')

pause(4)
close gcf;




