% Filename: UpdateRecords

function UpdateRecords(Records)
    set(findobj('tag','Player1WinsTB'), 'string' , ['W: ', num2str(Records(1))]);
    set(findobj('tag','Player2WinsTB'), 'string' , ['W: ', num2str(Records(2))]);
    set(findobj('tag', 'TiesTB') , 'string' , ['T: ', num2str(Records(3))]);
end