function SetPlayerNameBG(CurrentMove)
    if CurrentMove == 1
        set(findobj('tag', 'Player1NameTB'), 'BackgroundColor', [0 .8 0])
        set(findobj('tag', 'Player2NameTB'), 'BackgroundColor', [.94 .94 .94])
    elseif CurrentMove == 2
        set(findobj('tag', 'Player2NameTB'), 'BackgroundColor', [0 .8 0])
        set(findobj('tag', 'Player1NameTB'), 'BackgroundColor', [.94 .94 .94])
    else
        set(findobj('tag', 'Player1NameTB'), 'BackgroundColor', [.94 .94 .94])
        set(findobj('tag', 'Player2NameTB'), 'BackgroundColor', [.94 .94 .94])
    end
end