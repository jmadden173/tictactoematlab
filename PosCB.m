% Sets the current PB to inactive
set(gco, 'Enable', 'inactive')

% Get which PB the user played on
GameState(get(gco, 'UserData')) = CurrentMove;

% Displays the x's and o's on the push buttons
if CurrentMove == 1
    set(gco, 'string', 'x')
    CurrentMove = 2;
else
    set(gco, 'string', 'o')
    CurrentMove = 1;
end

% Update the UI with whos move it is
SetTurnTB(PlayerNames{CurrentMove});

% Sets the background on the player whos move it is currently
SetPlayerNameBG(CurrentMove);

winner = checkWinner(GameState);
% Check if there is a winner in the game
if winner ~= 0
    % Update the Records
    Records(winner) = Records(winner) + 1;
    % Update the UI
    set(findobj('tag', 'TurnTB'), 'string', [PlayerNames{winner}, ' Won']);
    UpdateRecords(Records);
    SetInactive();
    % Sets the background to default
    SetPlayerNameBG(0);
else
    % Check if there are ties in a game
    if all(GameState)
        % Update the records
        Records(3) = Records(3) + 1;
        % Update the UI
        set(findobj('tag', 'TurnTB'), 'string', 'Tie!');
        UpdateRecords(Records);
        % Sets the background to default
        SetPlayerNameBG(0);
        % NOTE: Set Inactive not needed since if there is a tie
        %       then it is assumed that everything is set to
        %       inactive
    end
end