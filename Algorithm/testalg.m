% Name:         Prob1.m
% Description:  Takes input from the user
%               on where to play in a game
%               of tictactoe and computes the
%               best possible move for the computer
%               to take and display the moves
%               using ascii art. The main driver
%               function for the minimax algorithim
% Input:        row: the row the user wants to play on
%               col: the col the user wants to play on
% Dependencies: minimax.m
%               getChildNodes.m
%               evalNode.m
%               dispBoard.m
%               checkWinner.m
%               checkTerminalNode.m
% Author:       John Madden
% Date:         10/9/2018

board =[
    0 0 2;...
    1 2 0;...
    0 1 0];

disp('Inital State');
dispBoard(board);
disp(' ');

index = 1;
for i=1:3
    for j=1:3
            if board(i,j)==0
                tmpboard = board;
                tmpboard(i,j)=1;
                tmpboards(:,:,index) = tmpboard;
                index = index + 1;
            end
    end
end

for i=1:size(tmpboards,3)
   tmpboardvalues(i) = minimax(tmpboards(:,:,i), 10, 1);
end

[value, index] = max(tmpboardvalues);

disp('Computers Move');
dispBoard(tmpboards(:,:,index))