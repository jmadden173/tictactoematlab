% Name:         evalNode.m
% Description:  Assigns a numerical value
%               if a plyers has won or not.
%               Used for computing the best
%               possible solution to tictactoe
%               using minimax algortithim
% Input:        node: a 3x3 matrix repesenting
%                   the playing board of a tictactoe
%                   game
% Output:       n/a
% Dependencies: checkWinner.m
% Author:       John Madden
% Date:         10/9/2018

function value = evalNode(node)
    winner = checkWinner(node);
    if (winner == 2)
        value = 100;
    elseif (winner == 1)
        value = -100;
    else
        value = 0;
%     else
%         % Make it so that move moves
%         % are less favorable
%         value = -1;
    end
end