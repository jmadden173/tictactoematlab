% Name:         checkWinner.m
% Desscription: Check if a player has won at
%               a game of tictactoe. Checks the
%               board for 0's and 1's using all
%               possible combinations of winning
%               plays
% Input:        node: a 3x3 matrix repesenting
%                   the playing board of a tictactoe
%                   game
% Output:       isWinnerNode: a integer representing
%                   the winning players
%                       0=nobody
%                       1=computer
%                       2=user
% Author:       John Madden
% Date:         10/9/2018

% Personal Notes:
% Outputs the value of the winning player
% 0 if no one is a winner
function [isWinnerNode] = checkWinner(node)
    isWinnerNode = 0;
    % Look for vertical wins
    for j = [1,4,7]
       if node(j) == node(j+1) && node(j) == node(j+2)
          isWinnerNode = node(j);
          return
       end
    end

    % Look for horizontal wins
    for k = [1,2,3]
       if node(k) == node(k+3) && node(k) == node(k+6)
           isWinnerNode = node(k);
           return
       end
    end

    % Look for diagnals
    % Top left to bottom right
    if node(1) == node(5) && node(1) == node(9)
        isWinnerNode = node(1);
        return
    end
    % Top right to bottom left
    if node(3) == node(5) && node(3) == node(7)
        isWinnerNode = node(3);
        return
    end
end