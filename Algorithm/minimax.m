% Name:         minimax.m
% Description:  Uses a minimax algorithim to
%               find the best possible move in
%               a game of tictactoe. Done with
%               recursion
% Input:        node: a 3x3 matrix reprenting the game board
%                   0 represents an empty space
%                   1 represents the computer move
%                   2 represents the users move
%               depth: the max depth for the algrothim to search
%                   through, recomended is 10
%               maximizingPlayer: a zero or a one that represents
%                   if it is the users move or the computers move
% Output:       nodevalue: a value that represents how good a move it,
%                   the more negative the better
% Requires:     getChildNodes.m
%               checkWinner.m
%               checkTerminalNode.m
%               evalNode.m
% Author:       John Madden
% Date:         10/9/2018

% Personal Notes:
% node = struct with child element
% depth = integer
% maximizngPlayer = bool
% 1 = maximizing player aka the computer
% 0 = minimiazng player aka the player
%
% 0 = empty space
% 2 = computer
% 1 = player
%
% compID is either a 1 or a 2 corresponding to the id of the computer
function [nodevalue] = minimax(node, depth, maximizingPlayer)
    if (depth == 0 | all(node) | checkWinner(node) ~= 0)
        nodevalue = evalNode(node);
        return
    end
    
    % Maximizing the computers score 
    if (maximizingPlayer == 1) % 1 is for the computer move
        nodevalue = -1 * Inf;
        child = getChildNodes(node, 2);
        for i = 1:size(child, 3)
            nodevalue = max(nodevalue, minimax(child(:,:,i), depth - 1, 0));
        end
        return
    % Minimizing the opponents score
    else % 0 is if it is the users move
        nodevalue = Inf;
        child = getChildNodes(node, 1);
        for i = 1:size(child, 3)
            nodevalue = min(nodevalue, minimax(child(:,:,i), depth - 1, 1));
        end
        return
    end
end