% Basically just an implementatin of testalg.m in a functino

% board is a 1x9 matrix
% depth is a double input
function [movePlayed] = bestMove(board, depth, minimaxHandle)
    if (any(board))
        playable = find(~board);
        tmpboards = zeros(3,3,length(playable));
        tmpboardvalues = zeros(1,length(playable));

        % Need an index to add to tmpboards
        index = 1;
        for i=playable
            if board(i)==0
                tmpboard = board;
                tmpboard(i)=2; % 2 is the computers identifier
                tmpboards(:,:,index) = reshape(tmpboard,3,3);
                index = index + 1;
            end
        end

        for i=1:size(tmpboards,3)
            tmpboardvalues(i) = minimaxHandle(tmpboards(:,:,i), depth, 0);
        end

        [~, movePlayed] = max(tmpboardvalues);

        movePlayed = playable(movePlayed);
    else
        plays = 1:2:9;
        movePlayed = plays(randi(length(plays), 1));
    end
end