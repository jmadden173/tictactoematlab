% Filename: StartMediumCB
% Purpose: This is the callback to the medium mode pushbutton

PlayerNames = {get(findobj('tag', 'NameEB'), 'string'), 'Computer'};

CurrentMove = randi(2);

close gcf;

open 'GameBoard.fig';



% -------------------------------------------------------------------------
% Play the computers move if they go first
% -------------------------------------------------------------------------
if CurrentMove == 2
    MoveMedium();
end



% Sets the callback of all PB's
SetCallback('PosMediumCB');


% Sets the callback of the new game button in GameBoard.fig
set(findobj('tag','NewGamePB'), 'Callback', 'GameBoardNewMediumCB');