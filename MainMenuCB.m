% Filename: MainMenuCB.m
% Purpose: Closes current figure and opens main menu figure

clear
close gcf
open MainMenu.fig