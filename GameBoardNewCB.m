% Filename: GameBoardNewCB.m
% Purpose: Resets the GameBoard

GameState = zeros(1,9);

% Clears the board and sets all PB to on
SetBlank();

% Sets the TurnTB on GameBoard.fig
SetTurnTB(PlayerNames{CurrentMove});