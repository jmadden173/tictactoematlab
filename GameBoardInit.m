% Zeros out the game state and the records
% when the gameboard is restarted
GameState = zeros(1, 9);
Records = zeros(1, 3);

% Update whos turn it is ib the UI
SetTurnTB(PlayerNames{CurrentMove});

% Update the score UI
UpdateRecords(Records);

% Sets the playernames for the GameBoard
set(findobj('tag', 'Player1NameTB'), 'string', PlayerNames{1});
set(findobj('tag', 'Player2NameTB'), 'string', PlayerNames{2});