% Filename: GameBoardNewCB.m
% Purpose: Resets the GameBoard

GameState = zeros(1,9);

% Clears the board and sets all PB to on
SetBlank();

% Checks whos turn it is on new game and continues accordingly
if CurrentMove == 1
    % Sets the TurnTB on GameBoard.fig
    SetTurnTB(PlayerNames{CurrentMove});
elseif CurrentMove == 2
    MoveMedium();
end